#!/bin/bash

APP="$1"
shift

if [ -z "$APP" ]; then
  if [ $(dirname $(pwd)) = "$HOME/workdir" ]; then
    APP=$(basename $(pwd))
  else
    echo "need an app to start"
    exit 1
  fi
fi

if [ "$APP" != "psql" -a "$APP" != "pg_restore" ]; then
  cd $HOME/workdir/$APP
  source ../environment
elif [ "$APP" = "pg_restore" ]; then
  source ../environment
  APP=pg_restore
else
  source ../environment
  APP=psql
fi

function start_stack() {
  stack exec $1;
}

function start_app() {
  trap 'make stop' EXIT

  if ! docker top mongo_container 2>/dev/null; then
    docker run -d --name mongo_container -p 27017:27017 --rm -v $(pwd)/appdata:/data/db mongo:3.6-hacker
    echo "Waiting until mongo is started"
    sleep 10
  fi

  make start
  make --directory=frontend/ start
}

function start_psql() {
  export PGPASSWORD=$POSTGRESQL_ADDON_PASSWORD
  psql -h $POSTGRESQL_ADDON_HOST -p $POSTGRESQL_ADDON_PORT -U $POSTGRESQL_ADDON_USER -d $POSTGRESQL_ADDON_DB
}

function start_pg_restore() {
  export PGPASSWORD=$POSTGRESQL_ADDON_PASSWORD
  pg_restore -h $POSTGRESQL_ADDON_HOST -p $POSTGRESQL_ADDON_PORT -U $POSTGRESQL_ADDON_USER -d $POSTGRESQL_ADDON_DB
}

case "$APP${FLAVOR:+-}$FLAVOR" in
  "psql") start_psql;;
  "pg_restore") start_pg_restore;;
  "app") start_app;;
  "admin-root") start_stack server;;
  "admin-user") start_stack server;;
  "carrier-directory") start_stack server;;
  "freight") start_stack freight-server;;
  "notifier") start_stack notifier-exe;;
  "notifier-amqp") start_stack notifier-exe-amqp;;
  "pricer") start_stack pricer;;
  "third-parties") start_stack server;;
  "toolbox") start_stack toolbox-api;;
esac
