#!/bin/bash

# gio is the replacement for xdg-open in gnome (and gtk) applications,
# including for instance Firefox. It makes two problems:
# First, it searches for terminals in a fixed list (which does not
# include lxterminal)
# Second, it will not use the configuration that was set using pcmanfm.

# This unwrapper will be called by gio (in place of gio-launch-desktop)
# thanks to the GIO_LAUNCH_DESKTOP variable, will try to find the opened
# file in the arguments, and use xdg-open to open it (beware, xdg-open
# might reuse gio in turn to open the file, for instance in gnome
# environments)

if [ -n "$GIO_LAUNCHED_DESKTOP_FILE" ]; then
  while [ -n "$1" -a "${1:0:1}" != "/" -a "${1:0:7}" != "file://" -a ! -f "$1" ]; do
    shift
  done
  xdg-open "$1"
else
  exec "$@"
fi
